<?php

$pdo = new PDO('mysql:host=127.0.0.1;dbname=test3', 'root', '');



switch ($_GET['action'])
{
  case 'add':
    $url = '/students.php?action=create';
    include 'forms/student.php';
  break;

  case 'create':
    $sql = $pdo->prepare('INSERT INTO `students` (`firstname`, `lastname`) VALUES (:firstname, :lastname)');
    $sql->execute([
      ':firstname' => $_POST['firstname'],
      ':lastname' => $_POST['lastname'],
    ]);
    echo 'Успешно!<br><a href="/students.php">Список студентов</a>';
  break;
  
  case 'edit':
    $sql = $pdo->prepare('SELECT * FROM `students` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $student = $sql->fetch();
    $url = '/students.php?action=update&id=' . $_GET['id'];
    include 'forms/student.php';
  break;
  
  case 'update':
    $sql = $pdo->prepare('UPDATE `students` SET `firstname` = :firstname, `lastname` = :lastname WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':firstname' => $_POST['firstname'],
      ':lastname' => $_POST['lastname'],
    ]);
    echo 'Успешно!<br><a href="/students.php">Список студентов</a>';
  break;

  case 'delete':
    $sql = $pdo->prepare('DELETE FROM `students` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/students.php">Список студентов</a>';
  break;

  default:

    echo '[ <a href="/">Вернуться на главную</a> ]<hr>';

    echo '<a href="/students.php?action=add">Добавить</a><br>';

    $students = $pdo->query('SELECT * FROM `students`');


    echo '<table border="1" cellspacing="0">';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Имя</th>';
    echo '<th>Фамилия</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($students as $student)
    {
      echo '<tr>';
      echo '<td>' . $student['id'] . '</td> ' 
      . '<td>' . $student['firstname'] . '</td> ' 
      . '<td>' . $student['lastname'] . '</td>'
      . '<td><a href="/students.php?action=edit&id=' . $student['id'] . '">ред.</a> <a href="/students.php?action=delete&id=' . $student['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

