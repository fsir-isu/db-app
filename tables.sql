CREATE TABLE `students` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(50) NOT NULL DEFAULT '',
  `lastname` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
);

CREATE TABLE `student_marks` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `student_id` INT(10) UNSIGNED NOT NULL,
  `subject_id` INT(10) UNSIGNED NOT NULL,
  `date` DATE NOT NULL,
  `mark` TINYINT(1) UNSIGNED NOT NULL DEFAULT '5',
  PRIMARY KEY (`id`),
  INDEX `student_id` (`student_id`),
  INDEX `subject_id` (`subject_id`),
  INDEX `student_subject` (`student_id`, `subject_id`)
);


CREATE TABLE `subjects` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
);
