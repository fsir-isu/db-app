<form action="<?= $url ?>" method="post">

  <label>Студент</label><br>
  <select name="student_id">
    <?php
    foreach ($students as $student)
    {
      echo '<option value="' . $student['id'] . '"' . ((int)$mark['student_id'] === (int)$student['id'] ? ' selected' : '') . '>';
      echo $student['firstname'] . ' ' . $student['lastname'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>

  <label>Предмет</label><br>
  <select name="subject_id">
    <?php
    foreach ($subjects as $subject)
    {
      echo '<option value="' . $subject['id'] . '"' . ((int)$mark['subject_id'] === (int)$subject['id'] ? ' selected' : '') . '>';
      echo $subject['name'];
      echo '</option>';
      echo "\n";
    }
    ?>
  </select><br>

  <label>Дата</label><br>
  <input type="date" name="date" value="<?= $mark['date'] ?: date('Y-m-d') ?>"><br>

  <label>Оценка</label><br>
  <select name="mark">
    <?php
    foreach ([1, 2, 3, 4, 5] as $int)
    {
      echo '<option value="' . $int . '"' . ($int === (int)$mark['mark'] ? ' selected' : '') . '>';
      echo $int;
      echo '</option>';
    }
    ?>
  </select><br>

  <br>
  <button type="submit">Сохранить</button>
</form>
