<?php


$pdo = new PDO('mysql:host=127.0.0.1;dbname=test3', 'root', '');



switch ($_GET['action'])
{
  case 'add':
    $url = '/subjects.php?action=create';
    include 'forms/subject.php';
  break;

  case 'create':
    $sql = $pdo->prepare('INSERT INTO `subjects` (`name`) VALUES (:name)');
    $sql->execute([
      ':name' => $_POST['name'],
    ]);
    echo 'Успешно!<br><a href="/subjects.php">Список предметов</a>';
  break;
  
  case 'edit':
    $sql = $pdo->prepare('SELECT * FROM `subjects` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $subject = $sql->fetch();
    $url = '/subjects.php?action=update&id=' . $_GET['id'];
    include 'forms/subject.php';
  break;
  
  case 'update':
    $sql = $pdo->prepare('UPDATE `subjects` SET `name` = :name WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':name' => $_POST['name'],
    ]);
    echo 'Успешно!<br><a href="/subjects.php">Список предметов</a>';
  break;

  case 'delete':
    $sql = $pdo->prepare('DELETE FROM `subjects` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/subjects.php">Список предметов</a>';
  break;

  default:

    echo '[ <a href="/">Вернуться на главную</a> ]<hr>';

    echo '<a href="/subjects.php?action=add">Добавить</a><br>';

    $subjects = $pdo->query('SELECT * FROM `subjects`');


    echo '<table border="1" cellspacing="0">';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Название</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($subjects as $subject)
    {
      echo '<tr>';
      echo '<td>' . $subject['id'] . '</td> ' 
      . '<td>' . $subject['name'] . '</td> ' 
      . '<td><a href="/subjects.php?action=edit&id=' . $subject['id'] . '">ред.</a> <a href="/subjects.php?action=delete&id=' . $subject['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

