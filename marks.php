<?php

$pdo = new PDO('mysql:host=127.0.0.1;dbname=test3', 'root', '');



switch ($_GET['action'])
{
  case 'add':
    $subjects = $pdo->query('SELECT * FROM `subjects`');
    $students = $pdo->query('SELECT * FROM `students`');

    $url = '/marks.php?action=create';
    include 'forms/mark.php';
  break;

  case 'create':
    $sql = $pdo->prepare('INSERT INTO `student_marks` (`subject_id`, `student_id`, `date`, `mark`) VALUES (:subject_id, :student_id, :date, :mark)');
    $sql->execute([
      ':subject_id' => (int)$_POST['subject_id'], 
      ':student_id' => (int)$_POST['student_id'], 
      ':date' => $_POST['date'], 
      ':mark' => (int)$_POST['mark']
    ]);
    echo 'Успешно добавлено!<br><a href="/marks.php">Успеваемость</a>';
  break;
  
  case 'edit':
    $sql = $pdo->prepare('SELECT * FROM `student_marks` WHERE `id` = :id');
    $sql->execute([':id' => $_GET['id']]);
    $mark = $sql->fetch();

    $subjects = $pdo->query('SELECT * FROM `subjects`');
    $students = $pdo->query('SELECT * FROM `students`');

    $url = '/marks.php?action=update&id=' . $_GET['id'];
    include 'forms/mark.php';
  break;
  
  case 'update':
    $sql = $pdo->prepare('UPDATE `student_marks` SET `subject_id` = :subject_id, `student_id` = :student_id, `date` = :date, `mark` = :mark WHERE `id` = :id LIMIT 1');
    $sql->execute([
      ':id' => $_GET['id'],
      ':subject_id' => (int)$_POST['subject_id'], 
      ':student_id' => (int)$_POST['student_id'], 
      ':date' => $_POST['date'], 
      ':mark' => (int)$_POST['mark']
    ]);
    echo 'Оценка успешно обновлена!<br><a href="/marks.php">Успеваемость</a>';
  break;

  case 'delete':
    $sql = $pdo->prepare('DELETE FROM `student_marks` WHERE `id` = :id LIMIT 1');
    $sql->execute([':id' => $_GET['id']]);
    echo 'Удалено!<br><a href="/marks.php">Успеваемость</a>';
  break;

  default:

    echo '[ <a href="/">Вернуться на главную</a> ]<hr>';

    echo '<a href="/marks.php?action=add">Добавить оценку</a><br>';

    $marks = $pdo->query('
      SELECT 
        `m`.`id`, 
        `m`.`date`, 
        `m`.`mark`, 
        CONCAT_WS(" ", `s`.`firstname`, `s`.`lastname`) `student_name`, 
        `j`.`name` `subject_name` 
      FROM 
        `student_marks` `m`, 
        `students` `s`, 
        `subjects` `j` 
      WHERE 
        `m`.`student_id` = `s`.`id` 
        AND
        `m`.`subject_id` = `j`.`id`
      ORDER BY `date` DESC');


    echo '<table border="1" cellspacing="0">';

    echo '<tr>';
    echo '<th>ID</th>';
    echo '<th>Студент</th>';
    echo '<th>Предмет</th>';
    echo '<th>Дата</th>';
    echo '<th>Оценка</th>';
    echo '<th>&nbsp;</th>';
    echo '</tr>';

    foreach ($marks as $mark)
    {
      echo '<tr>';
      echo '<td>' . $mark['id'] . '</td> ' 
      . '<td>' . $mark['student_name'] . '</td> ' 
      . '<td>' . $mark['subject_name'] . '</td> ' 
      . '<td>' . $mark['date'] . '</td> ' 
      . '<td>' . $mark['mark'] . '</td> ' 
      . '<td><a href="/marks.php?action=edit&id=' . $mark['id'] . '">ред.</a> <a href="/marks.php?action=delete&id=' . $mark['id'] . '">уд.</a></td>';
      echo '</tr>';

    }
    echo '</table>';

  break;

}

